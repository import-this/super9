//
//  Decodable+Extension.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 8/10/20.
//

import Foundation

fileprivate class Test {}

extension Decodable {
    static func mockData(forResource resource: String, withExtension ext: String) throws -> Data {
        return try! Data(contentsOf: Bundle(for: Test.self).url(forResource: resource, withExtension: ext)!)
    }
}
