//
//  MediaDetailsDataSourceTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class MediaDetailsDataSourceTests: XCTestCase {
    var mockPresenter = MockMediaDetailsPresenter()
    var mockSession = MockMediaDetailsSession()
    var mockDataTask = MockMediaDetailsDataTask()
    var dataSource: MediaDetailsDataSource!

    let config = Config(apiKey: "apiKey", apiVersion: "3", baseURL: "https://base-url.org/")

    let movieDetailsStub = try! Movie.mockData(
        forResource: "MovieResponse", withExtension: "json")
    let tvShowDetailsStub = try! TVShow.mockData(
        forResource: "TVShowResponse", withExtension: "json")

    // MARK: - Movie
    func testGivenSessionSuccess_WhenFetchDetailsForMovieIsCalled_ThenDetailsAreFetched() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expFail.isInverted = true

        // Given
        mockSession.mockData = movieDetailsStub
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .movie, withId: "mockId", onSuccess: { config in
            // Then
            expSucc.fulfill()
        }, onError: { error in
            XCTAssertNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionError_WhenFetchConfigIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockError = .generic
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .movie, withId: "mockId", onSuccess: { config in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenBadStatusCode_WhenFetchConfigIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockStatusCode = 400
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .movie, withId: "mockId", onSuccess: { config in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    // MARK: - TVShow
    func testGivenSessionSuccess_WhenFetchPopularMediaIsCalled_ThenPopularMediaAreFetched() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expFail.isInverted = true

        // Given
        mockSession.mockData = tvShowDetailsStub
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .tvShow, withId: "mockId", onSuccess: { media in
            // Then
            expSucc.fulfill()
        }, onError: { error in
            XCTAssertNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionError_WhenFetchPopularMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockError = .generic
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .tvShow, withId: "mockId", onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenBadStatusCode_WhenFetchPopularMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchDetails success")
        let expFail = expectation(description: "fetchDetails failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockStatusCode = 400
        dataSource = MediaDetailsDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchDetails(for: .tvShow, withId: "mockId", onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }
}
