//
//  MediaDetailsPresenterTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class MediaDetailsPresenterTests: XCTestCase {
    var mockDataSource = MockMediaDetailsDataSource()
    var mockView = MockMediaDetailsView()
    var presenter: MediaDetailsPresenter!

    var mockDetails = MediaDetails(
        title: "Interstellar",
        summary: "Interstellar summary",
        genre: "Adventure",
        backdropPath: "/backdrop-path",
        trailer: URL(string: "https://image.server.org/3/backdrop-path"),
        cast: [MediaDetails.Member(name: "Actor", character: "role", profilePath: "/profile-path")],
        similar: [MediaDetails.Similar(title: "Similar", posterPath: "/poster-path")])
    var mockDetailsUI = MediaDetailsUIModel(
        title: "Interstellar",
        summary: "Interstellar summary",
        genre: "Adventure",
        backdropURL: URL(string: "baseUrl/original/backdrop-path"),
        trailerAction: nil,  // Not included in Equatable
        cast: [
            MediaDetailsUIModel.Member(
                name: "Actor", character: "role", profileURL: URL(string: "baseUrl/original/profile-path"))
        ],
        similar: [
            MediaDetailsUIModel.Similar(
                title: "Similar", posterURL: URL(string: "baseUrl/original/poster-path"))
        ]
    )

    override func setUpWithError() throws {
        presenter = MediaDetailsPresenter(
            dataSource: mockDataSource,
            imageConfig: ImageConfig(
                baseUrl: "baseUrl/",
                backdropSizes: ["original"],
                posterSizes: ["original"],
                profileSizes: ["original"]))
        presenter.attach(mockView)
    }

    // MARK: - Lifecycle
    func testGivenDataSourceSuccessAndMediaIdAndType_WhenViewDidLoad_ThenDetailsAreDisplayed() {
        // Given
        let id = 1
        let type = MediaType.movie
        mockDataSource.success = true
        mockDataSource.details = mockDetails

        // When
        presenter.viewDidLoad(mediaId: id, mediaType: type)

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .display(details: mockDetailsUI)
        ])
        XCTAssertEqual(mockDataSource.actions, [
            .fetchDetails(for: type, withId: String(id))
        ])
    }

    func testGivenDataSourceError_WhenViewDidLoad_ThenErrorIsDisplayed() {
        // Given
        let id = 1
        let type = MediaType.movie
        mockDataSource.success = false

        // When
        presenter.viewDidLoad(mediaId: id, mediaType: type)

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .displayError(withMessage: MediaDetailsPresenter.Wordings.error.rawValue)
        ])
        XCTAssertEqual(mockDataSource.actions, [
            .fetchDetails(for: type, withId: String(id))
        ])
    }

    // MARK: - Actions
    // MARK: PlayTrailer
    func testGivenURL_WhenPlayTrailerIsCalled_ThenWebviewIsDisplayed() {
        // Given
        let url = URL(string: "localhost")!

        // When
        presenter.playTrailer(withURL: url)

        // Then
        XCTAssertEqual(mockView.actions, [.openWebview(withURL: url)])
        XCTAssertEqual(mockDataSource.actions, [])
    }
}
