//
//  MockMediaDetailsDataSource.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

@testable import Super9

class MockMediaDetailsDataSource: MediaDetailsDataSourceProtocol {
    // A generic error that the data source may produce.
    enum MockError: Swift.Error {
        case generic
    }

    enum Action: Equatable {
        case fetchDetails(for: MediaType, withId: String)
    }

    var actions = [Action]()

    var details = MediaDetails(
        title: "title",
        summary: "summary",
        genre: "genre",
        backdropPath: "backdrop-path",
        trailer: nil,
        cast: [],
        similar: [])

    var success = true

    func fetchDetails(
        for type: MediaType, withId id: String, onSuccess: @escaping (MediaDetails) -> Void, onError: @escaping (Error) -> Void
    ) {
        actions.append(.fetchDetails(for: type, withId: id))
        if success {
            onSuccess(details)
        } else {
            onError(MockError.generic)
        }
    }
}
