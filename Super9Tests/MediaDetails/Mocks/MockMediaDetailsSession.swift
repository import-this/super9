//
//  MockMediaDetailsSession.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import Foundation
@testable import Super9

class MockMediaDetailsDataTask: MediaDetailsDataTask {
    enum Action: Equatable {
        case resume
    }

    var actions = [Action]()

    func resume() {
        actions.append(.resume)
    }
}

class MockMediaDetailsSession: MediaDetailsSession {
    // A generic error that the session may return.
    enum Error: Swift.Error {
        case generic
    }

    enum Action: Equatable {
        case detailsDataTask(url: URL)
    }

    var actions = [Action]()

    var mockData: Data!
    var mockStatusCode: Int! = 200
    var mockError: Error! = nil

    var mockDataTask = MockMediaDetailsDataTask()

    func detailsDataTask(with url: URL, completionHandler: @escaping Handler) -> MediaDetailsDataTask {
        actions.append(.detailsDataTask(url: url))
        let mockURLResponse = HTTPURLResponse(
            url: url, statusCode: mockStatusCode, httpVersion: nil, headerFields: nil)
        defer {
            completionHandler(mockData, mockURLResponse, mockError)
        }
        return mockDataTask
    }
}
