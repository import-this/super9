//
//  MockMediaDetailsPresenter.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

@testable import Super9

class MockMediaDetailsPresenter: MediaDetailsPresenterProtocol {
    enum Action: Equatable {
        // MARK: Lifecycle
        case attach
        case viewDidLoad(mediaId: Int, mediaType: MediaType)
    }

    var actions = [Action]()

    // MARK: - Lifecycle
    func attach(_ view: MediaDetailsViewProtocol) {
        actions.append(.attach)
    }

    func viewDidLoad(mediaId: Int, mediaType: MediaType) {
        actions.append(.viewDidLoad(mediaId: mediaId, mediaType: mediaType))
    }
}
