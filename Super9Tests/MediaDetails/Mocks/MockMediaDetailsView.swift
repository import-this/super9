//
//  MockMediaDetailsView.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import Foundation
@testable import Super9

class MockMediaDetailsView: MediaDetailsViewProtocol {
    enum Action: Equatable {
        // MARK: Load
        case display(details: MediaDetailsUIModel)
        // MARK: Loader
        case showLoader
        case hideLoader
        // MARK: Alerts and Messages
        case displayError(withMessage: String)
        // MARK: Navigation
        case openWebview(withURL: URL)
    }

    var actions = [Action]()

    // MARK: - Load
    func display(details: MediaDetailsUIModel) {
        actions.append(.display(details: details))
    }

    // MARK: - Loader
    func showLoader() {
        actions.append(.showLoader)
    }

    func hideLoader() {
        actions.append(.hideLoader)
    }

    // MARK: - Alerts and Messages
    func displayError(withMessage message: String) {
        actions.append(.displayError(withMessage: message))
    }

    // MARK: - Navigation
    func openWebview(withURL url: URL) {
        actions.append(.openWebview(withURL: url))
    }
}
