//
//  MockSearchView.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

@testable import Super9

extension MediaDetailsUIModel: Equatable {
    public static func == (lhs: MediaDetailsUIModel, rhs: MediaDetailsUIModel) -> Bool {
        lhs.title == rhs.title &&
            lhs.summary == rhs.summary &&
            lhs.genre == rhs.genre &&
            lhs.backdropURL == rhs.backdropURL &&
            lhs.cast == rhs.cast &&
            lhs.similar == rhs.similar
    }
}

class MockSearchView: SearchViewProtocol {
    enum Action: Equatable {
        // MARK: Load
        case display(media: MediaUIModel)
        case display(noMedia: NoMediaUIModel)
        // MARK: Loader
        case showLoader
        case hideLoader
        // MARK: Alerts and Messages
        case displayError(withMessage: String)
    }

    var actions = [Action]()

    // MARK: - Load
    func display(media: MediaUIModel) {
        actions.append(.display(media: media))
    }

    func display(noMedia: NoMediaUIModel) {
        actions.append(.display(noMedia: noMedia))
    }

    // MARK: - Loader
    func showLoader() {
        actions.append(.showLoader)
    }

    func hideLoader() {
        actions.append(.hideLoader)
    }

    // MARK: - Alerts and Messages
    func displayError(withMessage message: String) {
        actions.append(.displayError(withMessage: message))
    }
}
