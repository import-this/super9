//
//  MockSearchDataSource.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

@testable import Super9

class MockSearchDataSource: SearchDataSourceProtocol {
    // A generic error that the data source may produce.
    enum MockError: Swift.Error {
        case generic
    }

    enum Action: Equatable {
        case fetchConfig
        case fetchPopularMedia
        case fetchMedia
    }

    var actions = [Action]()

    var imageConfig = ImageConfig(
        baseUrl: "baseUrl/",
        backdropSizes: ["original"],
        posterSizes: ["original"],
        profileSizes: ["original"])
    var media = [Media]()

    var success = true

    func fetchConfig(onSuccess: @escaping (ImageConfig) -> Void, onError: @escaping (Error) -> Void) {
        actions.append(.fetchConfig)
        if success {
            onSuccess(imageConfig)
        } else {
            onError(MockError.generic)
        }
    }

    func fetchPopularMedia(onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void) {
        actions.append(.fetchPopularMedia)
        if success {
            onSuccess(media)
        } else {
            onError(MockError.generic)
        }
    }

    func fetchMedia(forQuery query: String, onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void) {
        actions.append(.fetchMedia)
        if success {
            onSuccess(media)
        } else {
            onError(MockError.generic)
        }
    }
}
