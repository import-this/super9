//
//  MockSearchMediaSession.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import Foundation
@testable import Super9

class MockSearchMediaDataTask: SearchMediaDataTask {
    enum Action: Equatable {
        case resume
        case cancel
    }

    var actions = [Action]()

    func resume() {
        actions.append(.resume)
    }

    func cancel() {
        actions.append(.cancel)
    }
}

class MockSearchMediaSession: SearchMediaSession {
    // A generic error that the session may return.
    enum Error: Swift.Error {
        case generic
    }

    enum Action: Equatable {
        case mediaDataTask(url: URL)
    }

    var actions = [Action]()

    var mockData: Data!
    var mockStatusCode: Int! = 200
    var mockError: Error! = nil

    var mockDataTask = MockSearchMediaDataTask()

    func mediaDataTask(with url: URL, completionHandler: @escaping Handler) -> SearchMediaDataTask {
        actions.append(.mediaDataTask(url: url))
        let mockURLResponse = HTTPURLResponse(
            url: url, statusCode: mockStatusCode, httpVersion: nil, headerFields: nil)
        defer {
            completionHandler(mockData, mockURLResponse, mockError)
        }
        return mockDataTask
    }
}
