//
//  MockSearchPresenter.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

@testable import Super9

class MockSearchPresenter: SearchPresenterProtocol {
    enum Action: Equatable {
        // MARK: Lifecycle
        case attach
        case viewDidLoad
        // MARK: Actions
        case search(for: String)
        case refresh
    }

    var actions = [Action]()

    // MARK: - Lifecycle
    func attach(_ view: SearchViewProtocol) {
        actions.append(.attach)
    }

    func viewDidLoad() {
        actions.append(.viewDidLoad)
    }

    // MARK: - Actions
    func search(for item: String) {
        actions.append(.search(for: item))
    }

    func refresh() {
        actions.append(.refresh)
    }
}
