//
//  SearchPresenterTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class SearchPresenterTests: XCTestCase {

    var mockDataSource = MockSearchDataSource()
    var mockView = MockSearchView()
    var presenter: SearchPresenter!

    var mockMedia = [Media(
        type: .movie,
        id: 10,
        title: "Interstellar",
        posterPath: "/poster-path",
        voteAverage: 8,
        releaseDate: "2014-11-05".toFullDate()!,
        popularity: 1000)]
    var mockMediaUI = MediaUIModel(media: [
        MediumUIModel(
            id: 10,
            type: .movie,
            title: "Interstellar",
            posterURL: nil,
            voteAverage: "8.0",
            releaseDate: "2014-11-05")
    ])

    override func setUpWithError() throws {
        presenter = SearchPresenter(dataSource: mockDataSource)
        presenter.attach(mockView)
    }

    // MARK: - Lifecycle
    func testGivenDataSourceSuccessAndMedia_WhenViewDidLoad_ThenPopularMediaAreDisplayed() {
        // Given
        mockDataSource.success = true
        mockDataSource.media = mockMedia

        // When
        presenter.viewDidLoad()

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .showLoader,
            .hideLoader,
            .display(media: MediaUIModel(media: [
                MediumUIModel(
                    id: 10,
                    type: .movie,
                    title: "Interstellar",
                    posterURL: URL(string: "baseUrl/original/poster-path"),
                    voteAverage: "8.0",
                    releaseDate: "2014-11-05")
            ]))
        ])
        XCTAssertEqual(mockDataSource.actions, [
            .fetchConfig,
            .fetchPopularMedia
        ])
    }

    func testGivenDataSourceError_WhenViewDidLoad_ThenErrorIsDisplayed() {
        // Given
        mockDataSource.success = false

        // When
        presenter.viewDidLoad()

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .displayError(withMessage: SearchPresenter.Wordings.error.rawValue)
        ])
        XCTAssertEqual(mockDataSource.actions, [
            .fetchConfig
        ])
    }

    // MARK: - Actions
    // MARK: Search
    func testGivenEmptyItemAndDataSourceSuccess_WhenSearchingForItem_ThenPopularMediaAreDisplayed() {
        // Given
        mockDataSource.success = true
        mockDataSource.media = mockMedia
        for item in ["", " ", "   "] {
            mockView.actions = []
            mockDataSource.actions = []

            // When
            presenter.search(for: item)

            // Then
            XCTAssertEqual(mockView.actions, [
                .showLoader,
                .hideLoader,
                .display(media: mockMediaUI)
            ])
            XCTAssertEqual(mockDataSource.actions, [.fetchPopularMedia])
        }
    }

    func testGivenEmptyItemAndDataSourceError_WhenSearchingForItem_ThenErrorIsDisplayed() {
        // Given
        mockDataSource.success = false
        for item in ["", " ", "   "] {
            mockView.actions = []
            mockDataSource.actions = []

            // When
            presenter.search(for: item)

            // Then
            XCTAssertEqual(mockView.actions, [
                .showLoader,
                .hideLoader,
                .displayError(withMessage: SearchPresenter.Wordings.error.rawValue)
            ])
            XCTAssertEqual(mockDataSource.actions, [.fetchPopularMedia])
        }
    }

    func testGivenItemAndDataSourceSuccessAndMedia_WhenSearchingForItem_ThenMediaAreDisplayed() {
        // Given
        let item = "item"
        mockDataSource.success = true
        mockDataSource.media = mockMedia

        // When
        presenter.search(for: item)

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .display(media: mockMediaUI)
        ])
        XCTAssertEqual(mockDataSource.actions, [.fetchMedia])
    }

    func testGivenItemAndDataSourceSuccessAndNoMedia_WhenSearchingForItem_ThenNoMediaAreDisplayed() {
        // Given
        let item = "item"
        mockDataSource.success = true
        mockDataSource.media = []

        // When
        presenter.search(for: item)

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .display(noMedia: NoMediaUIModel(text: SearchPresenter.Wordings.noMedia.rawValue))
        ])
        XCTAssertEqual(mockDataSource.actions, [.fetchMedia])
    }

    func testGivenItemDataSourceError_WhenSearchingForItem_ThenErrorIsDisplayed() {
        // Given
        let item = "item"
        mockDataSource.success = false

        // When
        presenter.search(for: item)

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .displayError(withMessage: SearchPresenter.Wordings.error.rawValue)
        ])
        XCTAssertEqual(mockDataSource.actions, [.fetchMedia])
    }

    // MARK: Refresh
    func testGivenDataSourceSuccess_WhenRefreshing_ThenPopularMediaAreDisplayed() {
        // Given
        mockDataSource.success = true
        mockDataSource.media = mockMedia

        // When
        presenter.refresh()

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .display(media: mockMediaUI)
        ])
        XCTAssertEqual(mockDataSource.actions, [.fetchPopularMedia])
    }

    func testGivenDataSourceError_WhenRefreshing_ThenErrorIsDisplayed() {
        // Given
        mockDataSource.success = false

        // When
        presenter.refresh()

        // Then
        XCTAssertEqual(mockView.actions, [
            .showLoader,
            .hideLoader,
            .displayError(withMessage: SearchPresenter.Wordings.error.rawValue)
        ])
        XCTAssertEqual(mockDataSource.actions, [.fetchPopularMedia])
    }
}
