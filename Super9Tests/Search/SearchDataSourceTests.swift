//
//  SearchDataSourceTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class SearchDataSourceTests: XCTestCase {
    var mockPresenter = MockSearchPresenter()
    var mockSession = MockSearchMediaSession()
    var mockDataTask = MockSearchMediaDataTask()
    var dataSource: SearchDataSource!

    let config = Config(apiKey: "apiKey", apiVersion: "3", baseURL: "base-url/")

    let configurationDataStub = try! Configuration.mockData(
        forResource: "ConfigurationResponse", withExtension: "json")
    let popularMediaStub = try! PopularMovies.mockData(
        forResource: "PopularMoviesResponse", withExtension: "json")
    let searchMediaStub = try! SearchMulti.mockData(
        forResource: "SearchMultiResponse", withExtension: "json")

    func testGivenSessionSuccess_WhenFetchConfigIsCalled_ThenConfigIsFetched() {
        let expSucc = expectation(description: "fetchConfig success")
        let expFail = expectation(description: "fetchConfig failure")
        expFail.isInverted = true

        // Given
        mockSession.mockData = configurationDataStub
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchConfig(onSuccess: { config in
            // Then
            expSucc.fulfill()
        }, onError: { error in
            XCTAssertNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionError_WhenFetchConfigIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchConfig success")
        let expFail = expectation(description: "fetchConfig failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockError = .generic
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchConfig(onSuccess: { config in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenBadStatusCode_WhenFetchConfigIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchConfig success")
        let expFail = expectation(description: "fetchConfig failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockStatusCode = 400
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchConfig(onSuccess: { config in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionSuccess_WhenFetchPopularMediaIsCalled_ThenPopularMediaAreFetched() {
        let expSucc = expectation(description: "fetchPopularMedia success")
        let expFail = expectation(description: "fetchPopularMedia failure")
        expFail.isInverted = true

        // Given
        mockSession.mockData = popularMediaStub
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchPopularMedia(onSuccess: { media in
            // Then
            expSucc.fulfill()
        }, onError: { error in
            XCTAssertNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionError_WhenFetchPopularMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchPopularMedia success")
        let expFail = expectation(description: "fetchPopularMedia failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockError = .generic
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchPopularMedia(onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenBadStatusCode_WhenFetchPopularMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchPopularMedia success")
        let expFail = expectation(description: "fetchPopularMedia failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockStatusCode = 400
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchPopularMedia(onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionSuccess_WhenFetchMediaIsCalled_ThenMediaAreFetched() {
        let expSucc = expectation(description: "fetchMedia success")
        let expFail = expectation(description: "fetchMedia failure")
        expFail.isInverted = true

        // Given
        mockSession.mockData = searchMediaStub
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchMedia(forQuery: "query", onSuccess: { media in
            // Then
            expSucc.fulfill()
        }, onError: { error in
            XCTAssertNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenSessionError_WhenFetchMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchMedia success")
        let expFail = expectation(description: "fetchMedia failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockError = .generic
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchMedia(forQuery: "query", onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testGivenBadStatusCode_WhenFetchMediaIsCalled_ThenErrorIsReturned() {
        let expSucc = expectation(description: "fetchMedia success")
        let expFail = expectation(description: "fetchMedia failure")
        expSucc.isInverted = true

        // Given
        mockSession.mockStatusCode = 400
        dataSource = SearchDataSource(config: config, urlSession: mockSession)

        // When
        dataSource.fetchMedia(forQuery: "query", onSuccess: { media in
            expSucc.fulfill()
        }, onError: { error in
            // Then
            XCTAssertNotNil(error)
            expFail.fulfill()
        })

        waitForExpectations(timeout: 0.5, handler: nil)
    }
}
