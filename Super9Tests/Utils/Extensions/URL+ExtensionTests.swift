//
//  URL+ExtensionTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

//static func from(config: Config, path: API, queryItems: [URLQueryItem]) -> URL? {
//    from(config: config, path: path.rawValue, queryItems: queryItems)
//}



class URLExtensionTests: XCTestCase {
    func testGivenConfigAndPathAndQuery_WhenURLisMade_ThenCorrectURLisReturned() {
        // Given
        let config = Config(apiKey: "apiKey", apiVersion: "3", baseURL: "http://baseurl.com")
        let path = "/movie"
        let queryItems = [URLQueryItem(name: "name", value: "value")]

        // When
        let url = URL.from(config: config, path: path, queryItems: queryItems)

        // Then
        XCTAssertEqual(url?.absoluteString, "http://baseurl.com/3/movie?name=value&api_key=apiKey")
    }

    func testGivenConfigAndAPIAndQuery_WhenURLisMade_ThenCorrectURLisReturned() {
        // Given
        let config = Config(apiKey: "apiKey", apiVersion: "3", baseURL: "http://baseurl.com")
        let queryItems = [URLQueryItem(name: "name", value: "value")]
        for api in API.allCases {
            // When
            let url = URL.from(config: config, path: api, queryItems: queryItems)

            // Then
            XCTAssertEqual(url?.absoluteString, "http://baseurl.com/3\(api.rawValue)?name=value&api_key=apiKey")
        }
    }
}
