//
//  Date+ExtensionTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class DateExtensionTests: XCTestCase {
    private func makeDate(year: Int, month: Int, day: Int, hour: Int?, minute: Int?, second: Int?) -> Date {
        let calendar = Calendar.current
        let timeZone = TimeZone(identifier: "UTC")
        return DateComponents(
            calendar: calendar, timeZone: timeZone,
            year: year, month: month, day: day, hour: hour, minute: minute, second: second).date!
    }

    func testGivenDate_WhenFormattedToFullDate_ThenCorrectStringIsReturned() {
        let testSuite: [(date: Date, string: String)] = [
            (makeDate(year: 2020, month: 10, day: 8, hour: nil, minute: nil, second: nil), "2020-10-08"),
            (makeDate(year: 2020, month: 10, day: 25, hour: 5, minute: 4, second: 3), "2020-10-25"),
            (makeDate(year: 1, month: 1, day: 1, hour: 1, minute: 1, second: 1), "0001-01-01"),
            (makeDate(year: 2000, month: 1, day: 1, hour: 1, minute: 1, second: 1), "2000-01-01"),
            (makeDate(year: 2001, month: 12, day: 31, hour: 23, minute: 59, second: 59), "2001-12-31"),
        ]
        for (date, expected) in testSuite {
            // Given
            let date = date

            // When
            let fullDateString = date.toFullDateString()

            // Then
            XCTAssertEqual(fullDateString, expected)
        }
    }
}
