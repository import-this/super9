//
//  String+ExtensionTests.swift
//  Super9Tests
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import XCTest
@testable import Super9

class StringExtensionTests: XCTestCase {
    private func makeDate(year: Int, month: Int, day: Int) -> Date {
        let calendar = Calendar.current
        let timeZone = TimeZone(identifier: "UTC")
        return DateComponents(
            calendar: calendar, timeZone: timeZone, year: year, month: month, day: day).date!
    }

    func testGivenIsoFullDateString_WhenConvertedToDate_ThenCorrectDateIsReturned() {
        let testSuite: [(string: String, date: Date)] = [
            ("2020-10-08", makeDate(year: 2020, month: 10, day: 8)),
            ("2020-10-25", makeDate(year: 2020, month: 10, day: 25)),
            ("0001-01-01", makeDate(year: 1, month: 1, day: 1)),
            ("2000-01-01", makeDate(year: 2000, month: 1, day: 1)),
            ("2001-12-31", makeDate(year: 2001, month: 12, day: 31)),
            ("2020-12-13T10:10", makeDate(year: 2020, month: 12, day: 13)),
            ("2007-03-01T13:00:00Z", makeDate(year: 2007, month: 3, day: 1)),
        ]
        for (string, expected) in testSuite {
            // Given
            let string = string

            // When
            let date = string.toFullDate()

            // Then
            XCTAssertEqual(date, expected)
        }
    }

    func testGivenInvalidDateString_WhenConvertedToDate_ThenNilIsReturned() {
        let invalidStrings = [
            "",
            "2000",
            "2000-01",
            "200-10-32",
            "200-13-20",
            "2020-1-111",
        ]
        for string in invalidStrings {
            // Given
            let invalidString = string

            // When
            let date = invalidString.toFullDate()

            // Then
            XCTAssertNil(date)
        }
    }
}
