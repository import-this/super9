#  README

The following were implemented:
* A list (SearchViewController) of the current popular movies on TMDb (`/movie/popular`) is loaded when the app first loads.
* The user can refresh the list with a pull-to-refresh gesture.
* Above the list a standard search bar allows the user to search for both movies and TV shows (`/search/multi`). The following info is displayed:
    * Poster image.
    * Movie/TV show title.
    * Vote average (rating).
    * Release/First Air date.
* The user can tap on a movie or TV show to see more details (MediaDetailsViewController). The following details are displayed:
    * Backdrop image.
    * Movie/TV Show title.
    * Movie/TV show summary.
    * Genre (first object in array with genres).
    * Trailer (if available - for movie/TV show). Button opens a WKWebView and loads the corresponding YouTube page.
    * Cast (list of actors along with their character names and profile pictures).
    * Similar Movies/TV shows (Included in collection view).
* A loader is shown when loading lists and details.
* A Master-Detail UI (UISplitViewController) is used for better iPad support.

Implementation Details
* Both screens (Search and Media Details) are implemented using the MVP pattern.
* Business/Model logic is unit-tested.
* Images are cached to memory.
* Empty lists and errors (e.g. network) are handled gracefully.
* API key is stored in config.plist.
* Network responses are parsed using Decodable.

Task Improvements (for lack of time)
* A more aesthetically pleasing UI.
* More unit testing.
* More manual testing.
* Move Networking/Utils etc. code to separate modules.
* Cancel multiple requests that fire when searching.
* Correctly load images in UITableView/UICollectionView cells when scrolling and the network is slow. Possible solutions:
    * Check that the cell is still visible (`https://developer.apple.com/documentation/uikit/uitableview/1614983-cellforrow`).
    * An image loading pod like SDWebImage.
