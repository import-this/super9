//
//  DateFormatter+Extension.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import Foundation

extension DateFormatter {
    static let fullDateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
        return formatter
    }()
}
