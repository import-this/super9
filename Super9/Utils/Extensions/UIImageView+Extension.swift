//
//  UIImageView+Extension.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 5/10/20.
//

import Foundation
import UIKit

extension UIImageView {
    /// An image cache, mapping image URLs to images.
    static let cache = NSCache<NSString, UIImage>()
}

extension UIImageView {
    /// Loads the image from the URL specified and caches it in a static image cache.
    /// - Parameter url: The image URL.
    func loadImage(from url: URL?) {
        image = nil

        guard let url = url else {
            return
        }

        if let cachedImage = Self.cache.object(forKey: url.absoluteString as NSString) {
            self.image = cachedImage
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data,
                let image = UIImage(data: data)
            else {
                DispatchQueue.main.async { [weak self] in
                    self?.image = nil
                }
                return
            }

            Self.cache.setObject(image, forKey: url.absoluteString as NSString)
            DispatchQueue.main.async { [weak self] in
                self?.image = image
            }
        }.resume()
    }
}
