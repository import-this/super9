//
//  Date+Extension.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import Foundation

extension Date {
    func toFullDateString() -> String {
        DateFormatter.fullDateFormatter.string(from: self)
    }
}
