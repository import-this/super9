//
//  URL+Extension.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 5/10/20.
//

import Foundation

extension URL {
    static func from(config: Config, path: String, queryItems: [URLQueryItem] = []) -> URL? {
        var components = URLComponents(string: config.baseURL)
        components?.path = "/\(config.apiVersion)\(path)"
        components?.queryItems = queryItems + [URLQueryItem(name: "api_key", value: config.apiKey)]
        return components?.url
    }

    static func from(config: Config, path: API, queryItems: [URLQueryItem] = []) -> URL? {
        from(config: config, path: path.rawValue, queryItems: queryItems)
    }
}
