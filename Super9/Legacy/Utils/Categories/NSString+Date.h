//
//  NSString+Date.h
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 8/10/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Date)

- (nullable NSDate *)toFullDate;

@end

NS_ASSUME_NONNULL_END
