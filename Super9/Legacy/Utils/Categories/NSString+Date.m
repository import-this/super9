//
//  NSString+Date.m
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 8/10/20.
//

#import "NSString+Date.h"

@implementation NSString (Date)

- (NSDate *)toFullDate {
    NSISO8601DateFormatter* formatter = [NSISO8601DateFormatter new];
    formatter.formatOptions = NSISO8601DateFormatWithFullDate | NSISO8601DateFormatWithDashSeparatorInDate;
    return [formatter dateFromString:self];
}

@end
