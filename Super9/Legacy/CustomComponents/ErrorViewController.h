//
//  ErrorViewController.h
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 8/10/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ErrorViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

- (void)showIn:(UIViewController *)parent withError:(NSString *)error;

@end

NS_ASSUME_NONNULL_END
