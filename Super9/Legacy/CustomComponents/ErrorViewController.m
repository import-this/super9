//
//  ErrorViewController.m
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 8/10/20.
//

#import "ErrorViewController.h"

@implementation ErrorViewController

- (instancetype)init {
    self = [super initWithNibName:NSStringFromClass(ErrorViewController.self) bundle:nil];
    return self;
}

// MARK: Show
- (void)showIn:(UIViewController *)parent withError:(NSString *)error {
    [parent addChildViewController:self];
    self.view.frame = parent.view.frame;
    [parent.view addSubview:self.view];
    [self didMoveToParentViewController:parent];

    self.errorLabel.text = error;
}

@end
