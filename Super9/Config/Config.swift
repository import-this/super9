//
//  Config.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

struct Config: Codable {
    let apiKey: String
    let apiVersion: String
    let baseURL: String

    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case apiVersion = "api_version"
        case baseURL = "base_url"
    }
}

extension Config {
    static func parse() -> Config {
        let url = Bundle.main.url(forResource: "config", withExtension: "plist")!
        let data = try! Data(contentsOf: url)
        return try! PropertyListDecoder().decode(Config.self, from: data)
    }
}

extension Config {
    static let shared = parse()
}
