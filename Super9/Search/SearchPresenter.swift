//
//  SearchPresenter.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

// MARK: MediaUIModel
/// A UI model with info for a movie or a TV show.
struct MediumUIModel {
    let id: Int
    let type: MediaType
    let title: String
    let posterURL: URL?
    let voteAverage: String
    let releaseDate: String
}

/// A UI model with info for media to display.
struct MediaUIModel {
    let media: [MediumUIModel]
}

// MARK: NoMediaUIModel
/// A UI model with info used when there are no media to display.
struct NoMediaUIModel {
    let text: String
}

#if DEBUG
// Used for unit-testing only.
extension MediumUIModel: Equatable {}
extension MediaUIModel: Equatable {}
extension NoMediaUIModel: Equatable {}
#endif

// MARK: - SearchViewProtocol
protocol SearchViewProtocol: AnyObject {
    // MARK: Load
    func display(media: MediaUIModel)
    func display(noMedia: NoMediaUIModel)
    // MARK: Loader
    func showLoader()
    func hideLoader()
    // MARK: Alerts and Messages
    func displayError(withMessage message: String)
}

// MARK: - SearchDataSourceProtocol
protocol SearchDataSourceProtocol {
    func fetchConfig(onSuccess: @escaping (ImageConfig) -> Void, onError: @escaping (Error) -> Void)
    func fetchPopularMedia(onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void)
    func fetchMedia(forQuery query: String, onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void)
}

// MARK: - SearchPresenter
final class SearchPresenter {
    private let dataSource: SearchDataSourceProtocol

    weak private var view: SearchViewProtocol?

    private var imageConfig: ImageConfig?

    init(dataSource: SearchDataSourceProtocol = SearchDataSource()) {
        self.dataSource = dataSource
    }
}

// MARK: - Wordings
extension SearchPresenter {
    enum Wordings: String {
        case noMedia = "Hmm, nothing found.\nMaybe try something else."
        case error = "Oops! Something went wrong. Please try again later."
    }
}

// MARK: - Helpers
private extension SearchPresenter {
    func fetchConfig() {
        view?.showLoader()
        dataSource.fetchConfig(onSuccess: { [weak self] imageConfig in
            guard let self = self else { return }
            ImageConfig.shared = imageConfig
            self.imageConfig = imageConfig
            self.view?.hideLoader()
            self.fetchPopular()
        }, onError: { [weak self] _ in
            guard let self = self else { return }
            self.view?.hideLoader()
            self.view?.displayError(withMessage: Wordings.error.rawValue)
        })
    }

    func fetchPopular() {
        view?.showLoader()
        dataSource.fetchPopularMedia(onSuccess: { [weak self] media in
            guard let self = self else { return }
            self.view?.hideLoader()
            self.view?.display(media: MediaUIModel(
                media: media.sorted {
                    // Most popular first.
                    $0.popularity > $1.popularity
                }.map { [weak self] (item: Media) in
                    MediumUIModel(
                        id: item.id,
                        type: item.type,
                        title: item.title,
                        posterURL: self?.imageConfig?.makePosterURL(path: item.posterPath),
                        voteAverage: String(format: "%.1f", item.voteAverage),
                        releaseDate: item.releaseDate.toFullDateString())
                }
            ))
        }, onError: { [weak self] _ in
            guard let self = self else { return }
            self.view?.hideLoader()
            self.view?.displayError(withMessage: Wordings.error.rawValue)
        })
    }

    func fetchMedia(forQuery query: String) {
        view?.showLoader()
        dataSource.fetchMedia(forQuery: query, onSuccess: { [weak self] media in
            guard let self = self else { return }
            self.view?.hideLoader()

            guard media.count > 0 else {
                self.view?.display(noMedia: NoMediaUIModel(text: Wordings.noMedia.rawValue))
                return
            }

            self.view?.display(media: MediaUIModel(
                media: media.map { [weak self] (item: Media) in
                    MediumUIModel(
                        id: item.id,
                        type: item.type,
                        title: item.title,
                        posterURL: self?.imageConfig?.makePosterURL(path: item.posterPath),
                        voteAverage: String(format: "%.1f", item.voteAverage),
                        releaseDate: item.releaseDate.toFullDateString())
                }
            ))
        }, onError: { [weak self] _ in
            guard let self = self else { return }
            self.view?.hideLoader()
            self.view?.displayError(withMessage: Wordings.error.rawValue)
        })
    }
}

// MARK: - SearchPresenterProtocol Conformance
extension SearchPresenter: SearchPresenterProtocol {}

// MARK: Lifecycle
extension SearchPresenter {
    func attach(_ view: SearchViewProtocol) {
        assert(self.view == nil, "Cannot attach view twice")
        self.view = view
    }

    func viewDidLoad() {
        // Fetch the REST API configuration first. This is necessary for downloading images.
        // https://developers.themoviedb.org/3/configuration/get-api-configuration
        fetchConfig()
    }
}

// MARK: Actions
extension SearchPresenter {
    func search(for item: String) {
        // Strip out leading and trailing spaces.
        let item = item.trimmingCharacters(in: .whitespaces)
        if item.isEmpty {
            fetchPopular()
        } else {
            fetchMedia(forQuery: item)
        }
    }

    func refresh() {
        fetchPopular()
    }
}
