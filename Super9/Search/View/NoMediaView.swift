//
//  NoMediaView.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 6/10/20.
//

import UIKit

class NoMediaView: UIView {
    @IBOutlet private var contentView: NoMediaView!
    @IBOutlet private weak var textLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    private func setup() {
        let myType = type(of: self)
        guard let _ = Bundle(for: myType).loadNibNamed(String(describing: myType), owner: self) else {
            assertionFailure("Failed to load nib: \(String(describing: myType))")
            return
        }

        addSubview(contentView)

        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}

// MARK: - Load
extension NoMediaView {
    func configure(with text: String) {
        textLabel.text = text
    }
}
