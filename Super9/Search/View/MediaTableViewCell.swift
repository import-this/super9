//
//  MediaTableViewCell.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import UIKit

// MARK: MediaTableViewCell
final class MediaTableViewCell: UITableViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - Load
extension MediaTableViewCell {
    // MARK: Config
    /// A configuration object for `MediaTableViewCell`.
    struct Config {
        let image: URL?
        let title: String
        let rating: String
        let releaseDate: String
    }

    func configureView(with config: Config) {
        titleLabel.text = config.title
        ratingLabel.text = config.rating
        releaseDateLabel.text = config.releaseDate
        posterImageView.loadImage(from: config.image)
    }
}
