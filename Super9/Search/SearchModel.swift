//
//  SearchModel.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

// MARK: ImageConfig
struct ImageConfig {
    static var shared: ImageConfig?

    let baseUrl: String
    let backdropSizes: [String]
    let posterSizes: [String]
    let profileSizes: [String]
}

extension ImageConfig {
    // TODO: Select appropriate image sizes based on available view width.
    func makeImageURL(size: String, path: String) -> URL? {
        URL(string: "\(baseUrl)\(size)\(path)")
    }

    func makePosterURL(path: String?) -> URL? {
        guard let path = path, let posterSize = posterSizes.last else { return nil }
        return makeImageURL(size: posterSize, path: path)
    }

    func makeBackdropURL(path: String?) -> URL? {
        guard let path = path, let backdropSize = backdropSizes.last else { return nil }
        return makeImageURL(size: backdropSize, path: path)
    }

    func makeProfileURL(path: String?) -> URL? {
        guard let path = path, let profileSizes = profileSizes.first else { return nil }
        return makeImageURL(size: profileSizes, path: path)
    }
}

// MARK: Media
enum MediaType {
    case movie
    case tvShow
}

struct Media {
    let type: MediaType
    let id: Int
    let title: String
    let posterPath: String?
    let voteAverage: Double
    let releaseDate: Date
    let popularity: Double
}
