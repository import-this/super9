//
//  SearchDataSource.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

// MARK: Errors
enum SearchDataSourceError: Error {
    case generic
}

// MARK: - SearchMediaDataTask
protocol SearchMediaDataTask {
    func resume()
    func cancel()
}

extension URLSessionDataTask: SearchMediaDataTask {}

// MARK: - SearchMediaSession
protocol SearchMediaSession {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void

    func mediaDataTask(with url: URL, completionHandler: @escaping Handler) -> SearchMediaDataTask
}

extension URLSession: SearchMediaSession {
    func mediaDataTask(with url: URL, completionHandler: @escaping Handler) -> SearchMediaDataTask {
        dataTask(with: url, completionHandler: completionHandler)
    }
}

// MARK: - Mappers
extension PopularMovies {
    func toMediaList() -> [Media] {
        (results ?? []).compactMap { movie in
            guard
                let id = movie.id,
                let title = movie.title,
                let voteAverage = movie.voteAverage,
                let releaseDate = movie.releaseDate?.toFullDate(),
                let popularity = movie.popularity
            else {
                // Discard partial results.
                return nil
            }
            return Media(
                type: .movie,
                id: id,
                title: title,
                posterPath: movie.posterPath,
                voteAverage: voteAverage,
                releaseDate: releaseDate,
                popularity: popularity)
        }
    }
}

extension SearchMulti {
    func toMediaList() -> [Super9.Media] {
        // Discards partial results.
        (results ?? []).compactMap { media in
            switch media {
            case .movie(let movie):
                guard
                    let id = movie.id,
                    let title = movie.title,
                    let voteAverage = movie.voteAverage,
                    let releaseDate = movie.releaseDate?.toFullDate(),
                    let popularity = movie.popularity
                else {
                    return nil
                }
                return Super9.Media(
                    type: .movie,
                    id: id,
                    title: title,
                    posterPath: movie.posterPath,
                    voteAverage: voteAverage,
                    releaseDate: releaseDate,
                    popularity: popularity)
            case .tvShow(let tvShow):
                guard
                    let id = tvShow.id,
                    let title = tvShow.name,
                    let voteAverage = tvShow.voteAverage,
                    let releaseDate = tvShow.firstAirDate?.toFullDate(),
                    let popularity = tvShow.popularity
                else {
                    return nil
                }
                return Super9.Media(
                    type: .tvShow,
                    id: id,
                    title: title,
                    posterPath: tvShow.posterPath,
                    voteAverage: voteAverage,
                    releaseDate: releaseDate,
                    popularity: popularity)
            case .person:
                return nil
            }
        }
    }
}

// MARK: - SearchDataSource
final class SearchDataSource: SearchDataSourceProtocol {
    private let config: Config
    private let urlSession: SearchMediaSession

    private var fetchMoviesDataTask: SearchMediaDataTask?

    init(config: Config = Config.shared, urlSession: SearchMediaSession = URLSession.shared) {
        self.config = config
        self.urlSession = urlSession
    }

    func fetchConfig(onSuccess: @escaping (ImageConfig) -> Void, onError: @escaping (Error) -> Void) {
        let configURL = URL.from(config: config, path: .config)
        guard let url = configURL else {
            onError(SearchDataSourceError.generic)
            return
        }
        urlSession.mediaDataTask(with: url) { data, response, error in
            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data, let config = try? JSONDecoder().decode(Configuration.self, from: data),
                let images = config.images,
                let baseUrl = images.secureBaseUrl,
                let backdropSizes = images.backdropSizes,
                let posterSizes = images.posterSizes,
                let profileSizes = images.profileSizes
            else {
                DispatchQueue.main.async {
                    onError(SearchDataSourceError.generic)
                }
                return
            }
            let imageConfig = ImageConfig(
                baseUrl: baseUrl,
                backdropSizes: backdropSizes,
                posterSizes: posterSizes,
                profileSizes: profileSizes
            )
            DispatchQueue.main.async {
                onSuccess(imageConfig)
            }
        }.resume()
    }

    func fetchPopularMedia(onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void) {
        let popularURL = URL.from(config: config, path: .popular, queryItems: [
            URLQueryItem(name: "page", value: "1")
        ])
        guard let url = popularURL else {
            onError(SearchDataSourceError.generic)
            return
        }

        urlSession.mediaDataTask(with: url) { data, response, error in
            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data, let response = try? JSONDecoder().decode(PopularMovies.self, from: data)
            else {
                DispatchQueue.main.async {
                    onError(SearchDataSourceError.generic)
                }
                return
            }

            let media = response.toMediaList()
            DispatchQueue.main.async {
                onSuccess(media)
            }
        }.resume()
    }

    func fetchMedia(forQuery query: String, onSuccess: @escaping ([Media]) -> Void, onError: @escaping (Error) -> Void) {
        let searchURL = URL.from(config: config, path: .search, queryItems: [
            URLQueryItem(name: "query", value: query),
            URLQueryItem(name: "page", value: "1")
        ])
        guard let url = searchURL else {
            onError(SearchDataSourceError.generic)
            return
        }

        fetchMoviesDataTask?.cancel()
        fetchMoviesDataTask = urlSession.mediaDataTask(with: url) { [weak self] data, response, error in
            defer {
              self?.fetchMoviesDataTask = nil
            }

            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data, let response = try? JSONDecoder().decode(SearchMulti.self, from: data)
            else {
                DispatchQueue.main.async {
                    onError(SearchDataSourceError.generic)
                }
                return
            }

            let media = response.toMediaList()
            DispatchQueue.main.async {
                onSuccess(media)
            }
        }
        fetchMoviesDataTask?.resume()
    }
}
