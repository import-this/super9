//
//  SearchViewController.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import UIKit

// MARK: SearchPresenterProtocol
protocol SearchPresenterProtocol: AnyObject {
    // MARK: Lifecycle
    func attach(_ view: SearchViewProtocol)
    func viewDidLoad()
    // MARK: Actions
    func search(for item: String)
    func refresh()
}

// MARK: - SearchViewController
final class SearchViewController: UITableViewController {
    private lazy var activityIndicatorController = ActivityIndicatorViewController()
    private lazy var noMediaView = NoMediaView()

    private var movieDetailsViewController: MediaDetailsViewController? = nil

    private let presenter: SearchPresenterProtocol

    private var media = [MediumUIModel]()


    required init(presenter: SearchPresenterProtocol = SearchPresenter()) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)

        presenter.attach(self)
    }

    required init?(coder: NSCoder) {
        self.presenter = SearchPresenter()
        super.init(coder: coder)

        presenter.attach(self)
    }
}

// MARK: - Segues
extension SearchViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard
            segue.identifier == "showDetail",
            let indexPath = tableView.indexPathForSelectedRow,
            let controller = (segue.destination as? UINavigationController)?.topViewController as? MediaDetailsViewController
        else {
            return
        }
        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
        controller.navigationItem.leftItemsSupplementBackButton = true
        let medium = media[indexPath.row]
        controller.media = .init(
            id: medium.id,
            type: medium.type)
        movieDetailsViewController = controller
    }
}

// MARK: - Wordings
extension SearchViewController {
    enum Wordings: String {
        case fetching = "Fetching the latest Movies and TV shows..."
    }
}

// MARK: - Lifecycle
extension SearchViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = splitViewController {
            let controllers = split.viewControllers
            movieDetailsViewController = (controllers[controllers.count-1] as! UINavigationController)
                .topViewController as? MediaDetailsViewController
        }

        // Self-sizing table rows.
        tableView.rowHeight = UITableView.automaticDimension
        // Height estimates are necessary because the rows are self-sizing.
        tableView.estimatedRowHeight = 600

        // No separators empty rows.
        tableView.tableFooterView = UIView()

        addSearch()
        addRefresh()

        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
}

// MARK: - Search
private extension SearchViewController {
    func addSearch() {
        // Results will be displayed in the same `SearchViewController`.
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        // No dimming necessary, since the activity indicator will hide it.
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.placeholder = "Search for movies and TV shows"
        //searchController.searchBar.returnKeyType = .done
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            // Scrolling can be extensive due to poster images. Always show search bar.
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            navigationItem.titleView = searchController.searchBar
        }
        // Ensure that the search bar doesn’t remain on the screen if the user navigates
        // to another view controller while the `UISearchController` is active.
        definesPresentationContext = true
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        presenter.search(for: text)
    }
}

// MARK: - Refresh
private extension SearchViewController {
    func addRefresh() {
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl?.attributedTitle = NSAttributedString(string: Wordings.fetching.rawValue)
    }

    @objc func refresh() {
        presenter.refresh()
    }
}

// MARK: - TableView
extension SearchViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        media.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: MediaTableViewCell.self)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? MediaTableViewCell else {
            assertionFailure("Expected `\(MediaTableViewCell.self)` type for reuse identifier \(identifier)")
            return MediaTableViewCell()
        }
        let movieOrShow = media[indexPath.row]
        cell.configureView(with: MediaTableViewCell.Config(
            image: movieOrShow.posterURL,
            title: movieOrShow.title,
            rating: movieOrShow.voteAverage,
            releaseDate: movieOrShow.releaseDate)
        )
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        false
    }
}

// MARK: - SearchViewProtocol Conformance
extension SearchViewController: SearchViewProtocol {}

// MARK: Load
extension SearchViewController {
    func display(media: MediaUIModel) {
        self.media = media.media
        refreshControl?.endRefreshing()
        tableView.reloadData()
        tableView.backgroundView = nil
    }

    func display(noMedia: NoMediaUIModel) {
        self.media = []
        refreshControl?.endRefreshing()
        tableView.reloadData()
        // A view showing that no offer is available. The background view appears behind
        // all rows, header and footer views, so will show when the offer list is empty.
        noMediaView.configure(with: noMedia.text)
        tableView.backgroundView = noMediaView
    }
}

// MARK: Loader
extension SearchViewController {
    func showLoader() {
        activityIndicatorController.start(in: self)
    }

    func hideLoader() {
        activityIndicatorController.stop()
    }
}

// MARK: Alerts and Messages
extension SearchViewController {
    func displayError(withMessage message: String) {
        display(noMedia: NoMediaUIModel(text: message))
    }
}
