//
//  Model.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

// MARK: Configuration
/// GET /configuration
struct Configuration: Decodable {
    struct Images: Decodable {
        let baseUrl: String?
        let secureBaseUrl: String?
        let backdropSizes: [String]?
        let posterSizes: [String]?
        let profileSizes: [String]?

        private enum CodingKeys: String, CodingKey {
            case baseUrl = "base_url"
            case secureBaseUrl = "secure_base_url"
            case backdropSizes = "backdrop_sizes"
            case posterSizes = "poster_sizes"
            case profileSizes = "profile_sizes"
        }
    }

    let images: Images?

    private enum CodingKeys: String, CodingKey {
        case images
    }
}

// MARK: - Popular Movies
/// GET /movie/popular
struct PopularMovies: Decodable {
    // MARK: Movie
    struct Movie: Decodable {
        let id: Int?
        let title: String?
        let releaseDate: String?
        let posterPath: String?
        let voteAverage: Double?
        let popularity: Double?

        private enum CodingKeys: String, CodingKey {
            case id
            case title
            case releaseDate = "release_date"
            case posterPath = "poster_path"
            case voteAverage = "vote_average"
            case popularity
        }
    }

    let results: [Movie]?
}

// MARK: - SearchMulti
/// GET /search/multi
struct SearchMulti: Decodable {
    // MARK: Movie
    struct Movie: Decodable {
        let mediaType: String   // Required - Allowed Values: movie
        let id: Int?
        let title: String?
        let releaseDate: String?
        let posterPath: String?
        let voteAverage: Double?
        let popularity: Double?

        private enum CodingKeys: String, CodingKey {
            case mediaType = "media_type"
            case id
            case title
            case releaseDate = "release_date"
            case posterPath = "poster_path"
            case voteAverage = "vote_average"
            case popularity
        }
    }

    // MARK: TVShow
    struct TVShow: Decodable {
        let mediaType: String   // Required - Allowed Values: tv
        let id: Int?
        let name: String?
        let firstAirDate: String?
        let posterPath: String?
        let voteAverage: Double?
        let popularity: Double?

        private enum CodingKeys: String, CodingKey {
            case mediaType = "media_type"
            case id
            case name
            case firstAirDate = "first_air_date"
            case posterPath = "poster_path"
            case voteAverage = "vote_average"
            case popularity
        }
    }

    // MARK: Person
    struct Person: Decodable {
        let mediaType: String   // Required - Allowed Values: person
        // Not gonna obtain any info about people for now.

        private enum CodingKeys: String, CodingKey {
            case mediaType = "media_type"
        }
    }

    // MARK: Media
    enum Media: Decodable {
        enum MediaType: String {
            case movie
            case tv
            case person
        }

        case movie(Movie)
        case tvShow(TVShow)
        case person(Person)

        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            // Check the `media_type` to make sure this it the correct type.
            // (Partial responses due to optionals may match more than one type.)
            if let movie = try? container.decode(Movie.self), movie.mediaType == MediaType.movie.rawValue {
                self = .movie(movie)
            } else if let tvShow = try? container.decode(TVShow.self), tvShow.mediaType == MediaType.tv.rawValue {
                self = .tvShow(tvShow)
            } else if let person = try? container.decode(Person.self), person.mediaType == MediaType.person.rawValue {
                self = .person(person)
            } else {
                throw DecodingError.typeMismatch(Media.self, DecodingError.Context(
                    codingPath: decoder.codingPath, debugDescription: "Wrong type for `Media`"))
            }
        }
    }

    let results: [Media]?
}

// MARK: - Movie Details
/// GET /movie/{movie_id}?append_to_response=videos,credits,similar
/// https://developers.themoviedb.org/3/getting-started/append-to-response
struct Movie: Decodable {
    struct Genre: Decodable {
        let id: Int?
        let name: String?
    }

    // MARK: Videos
    /// GET /movie/{movie_id}/videos
    struct Videos: Decodable {
        struct Video: Decodable {
            let key: String
            let site: String
            let type: String
        }

        let results: [Video]?
    }

    // MARK: Credits
    /// GET /movie/{movie_id}/credits
    struct Credits: Decodable {
        struct Cast: Decodable {
            let character: String?
            let name: String?
            let profilePath: String?
            let order: Int?

            private enum CodingKeys: String, CodingKey {
                case character
                case name
                case profilePath = "profile_path"
                case order
            }
        }

        let cast: [Cast]?
    }

    // MARK: Similar
    /// GET /movie/{movie_id}/similar
    struct Similar: Decodable {
        struct Movie: Decodable {
            let title: String?
            let posterPath: String?

            private enum CodingKeys: String, CodingKey {
                case title
                case posterPath = "poster_path"
            }
        }

        let results: [Movie]?
    }

    let backdropPath: String?
    let genres: [Genre]?
    let overview: String?
    let posterPath: String?
    let title: String?

    let videos: Videos?
    let credits: Credits?
    let similar: Similar?

    private enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case genres
        case overview
        case posterPath = "poster_path"
        case title
        case videos
        case credits
        case similar
    }
}

// MARK: - TVShow Details
/// GET /tv/{tv_id}?append_to_response=videos,credits,similar
/// https://developers.themoviedb.org/3/getting-started/append-to-response
struct TVShow: Decodable {
    struct Genre: Decodable {
        let id: Int?
        let name: String?
    }

    // MARK: Videos
    /// GET /tv/{tv_id}/videos
    struct Videos: Decodable {
        struct Video: Decodable {
            let key: String
            let site: String
            let type: String
        }

        let results: [Video]?
    }

    // MARK: Credits
    /// GET /tv/{tv_id}/credits
    struct Credits: Decodable {
        struct Cast: Decodable {
            let character: String?
            let name: String?
            let profilePath: String?
            let order: Int?

            private enum CodingKeys: String, CodingKey {
                case character
                case name
                case profilePath = "profile_path"
                case order
            }
        }

        let cast: [Cast]?
    }

    // MARK: Similar
    /// GET /tv/{tv_id}/similar
    struct Similar: Decodable {
        struct TVShow: Decodable {
            let title: String?
            let posterPath: String?

            private enum CodingKeys: String, CodingKey {
                case title
                case posterPath = "poster_path"
            }
        }

        let results: [TVShow]?
    }

    let backdropPath: String?
    let genres: [Genre]?
    let name: String?
    let overview: String?
    let posterPath: String?

    let videos: Videos?
    let credits: Credits?
    let similar: Similar?

    private enum CodingKeys: String, CodingKey {
        case backdropPath = "backdrop_path"
        case genres
        case name
        case overview
        case posterPath = "poster_path"
        case videos
        case credits
        case similar
    }
}
