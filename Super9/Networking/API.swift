//
//  API.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

enum API: String {
    case config = "/configuration"
    case search = "/search/multi"
    case popular = "/movie/popular"
    case movie = "/movie/"
    case tv = "/tv/"
}

#if DEBUG
// Used for unit-testing only.
extension API: CaseIterable {}
#endif
