//
//  MediaDetailsViewController.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import UIKit
import AVKit
import AVFoundation

// MARK: Extension Helpers
extension UITableView {
    /// Returns a reusable table-view cell object of the specified generic type and adds it to the table.
    /// The type name is used as the reuse identifier for the table-view cell.
    /// - Parameters:
    ///   - indexPath: The index path specifying the location of the cell.
    func dequeueCell<Cell: UITableViewCell>(for indexPath: IndexPath) -> Cell {
        let identifier = String(describing: Cell.self)
        guard let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? Cell else {
            assertionFailure("Expected `\(Cell.self)` type for reuse identifier \(identifier)")
            return Cell()
        }
        return cell
    }
}

// MARK: - MediaDetailsPresenterProtocol
protocol MediaDetailsPresenterProtocol: AnyObject {
    // MARK: Lifecycle
    func attach(_ view: MediaDetailsViewProtocol)
    func viewDidLoad(mediaId: Int, mediaType: MediaType)
}

// MARK: - MediaDetailsViewController
class MediaDetailsViewController: UITableViewController {
    @IBOutlet weak var detailDescriptionLabel: UILabel!

    private lazy var activityIndicatorController = ActivityIndicatorViewController()
    private lazy var errorController = ErrorViewController()

    private let presenter: MediaDetailsPresenterProtocol

    struct Media {
        // The movie or TV show ID. This will be used to query the API and retrieve the movie/TV show details.
        let id: Int
        let type: MediaType
    }

    var media: Media? {
        didSet {
            guard let media = media else { return }
            presenter.viewDidLoad(mediaId: media.id, mediaType: media.type)
        }
    }

    private var details: MediaDetailsUIModel = MediaDetailsUIModel(
        title: "",
        summary: "",
        genre: nil,
        backdropURL: nil,
        trailerAction: nil,
        cast: [],
        similar: [])

    required init(presenter: MediaDetailsPresenterProtocol = MediaDetailsPresenter()) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)

        presenter.attach(self)
    }

    required init?(coder: NSCoder) {
        self.presenter = MediaDetailsPresenter()
        super.init(coder: coder)

        presenter.attach(self)
    }
}

// MARK: - Lifecycle
extension MediaDetailsViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // Self-sizing table rows.
        tableView.rowHeight = UITableView.automaticDimension
        // Height estimates are necessary because the rows are self-sizing.
        tableView.estimatedRowHeight = 500

        // No separators empty rows.
        tableView.tableFooterView = UIView()
    }
}

// MARK: - TableView
extension MediaDetailsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return details.cast.count
        case 2: return 1
        default: return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: MediaDetailsTableViewCell = tableView.dequeueCell(for: indexPath)
            cell.configureView(with: MediaDetailsTableViewCell.Config(
                title: details.title,
                summary: details.summary,
                genre: details.genre,
                backdropImage: details.backdropURL,
                playTrailerAction: details.trailerAction)
            )
            return cell
        case 1:
            let cell: CastTableViewCell = tableView.dequeueCell(for: indexPath)
            let member = details.cast[indexPath.row]
            cell.configureView(with: member)
            return cell
        case 2:
            let cell: SimilarMediaTableViewCell = tableView.dequeueCell(for: indexPath)
            let similar = details.similar
            cell.configureView(with: similar)
            return cell
        default:
            assertionFailure()
            return UITableViewCell()
        }
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        false
    }
}

// MARK: - MediaDetailsViewProtocol Conformance
extension MediaDetailsViewController: MediaDetailsViewProtocol {}

// MARK: Load
extension MediaDetailsViewController {
    func display(details: MediaDetailsUIModel) {
        self.details = details
        tableView.reloadData()
    }
}

// MARK: Loader
extension MediaDetailsViewController {
    func showLoader() {
        activityIndicatorController.start(in: self)
    }

    func hideLoader() {
        activityIndicatorController.stop()
    }
}

// MARK: Alerts and Messages
extension MediaDetailsViewController {
    func displayError(withMessage message: String) {
        errorController.show(in: self, withError: message)
    }
}

// MARK: Navigation
extension MediaDetailsViewController {
//    func playVideo(withURL url: URL) {
//        let player = AVPlayer(url: url)
//        let playerController = AVPlayerViewController()
//        playerController.player = player
//        // Modally present the player and call the player's play() method when complete.
//        present(playerController, animated: true) {
//            player.play()
//        }
//    }

    func openWebview(withURL url: URL) {
        let webviewController = WebviewViewController(url: url)
        present(webviewController, animated: true)
    }
}
