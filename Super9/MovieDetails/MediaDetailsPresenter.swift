//
//  MediaDetailsPresenter.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

struct MediaDetailsUIModel {
    struct Member {
        let name: String
        let character: String
        let profileURL: URL?
    }

    struct Similar {
        let title: String
        let posterURL: URL?
    }

    let title: String
    let summary: String
    let genre: String?
    let backdropURL: URL?
    let trailerAction: (() -> Void)?
    let cast: [Member]
    let similar: [Similar]
}

#if DEBUG
// Used for unit-testing only.
extension MediaDetailsUIModel.Member: Equatable {}
extension MediaDetailsUIModel.Similar: Equatable {}
#endif

// MARK: MediaDetailsViewProtocol
protocol MediaDetailsViewProtocol: AnyObject {
    // MARK: Load
    func display(details: MediaDetailsUIModel)
    // MARK: Loader
    func showLoader()
    func hideLoader()
    // MARK: Alerts and Messages
    func displayError(withMessage message: String)
    // MARK: Navigation
    func openWebview(withURL: URL)
}

// MARK: - MediaDetailsDataSourceProtocol
protocol MediaDetailsDataSourceProtocol {
    func fetchDetails(
        for: MediaType, withId: String, onSuccess: @escaping (MediaDetails) -> Void, onError: @escaping (Error) -> Void
    )
}

// MARK: - MediaDetailsPresenter
final class MediaDetailsPresenter {
    private let dataSource: MediaDetailsDataSourceProtocol

    weak private var view: MediaDetailsViewProtocol?

    private var imageConfig: ImageConfig?

    init(dataSource: MediaDetailsDataSourceProtocol = MediaDetailsDataSource(),
         imageConfig: ImageConfig? = ImageConfig.shared) {
        self.dataSource = dataSource
        self.imageConfig = imageConfig
    }
}

// MARK: - Wordings
extension MediaDetailsPresenter {
    enum Wordings: String {
        case error = "Oops! Something went wrong. Please try again later"
    }
}

// MARK: - Helpers
private extension MediaDetailsPresenter {
    func fetch(mediaWithId id: String, andType type: MediaType) {
        view?.showLoader()
        dataSource.fetchDetails(for: type, withId: id, onSuccess: { [weak self] details in
            self?.view?.hideLoader()
            self?.view?.display(details: MediaDetailsUIModel(
                title: details.title,
                summary: details.summary,
                genre: details.genre,
                backdropURL: self?.imageConfig?.makeBackdropURL(path: details.backdropPath),
                trailerAction: { [weak self] in
                    self?.playTrailer(withURL: details.trailer)
                },
                cast: details.cast.map { member in
                    MediaDetailsUIModel.Member(
                        name: member.name,
                        character: member.character,
                        profileURL: self?.imageConfig?.makeProfileURL(path: member.profilePath))
                },
                similar: details.similar.map { similar in
                    MediaDetailsUIModel.Similar(
                        title: similar.title,
                        posterURL: self?.imageConfig?.makePosterURL(path: similar.posterPath))
                }
            ))
        }, onError: { [weak self] _ in
            self?.view?.hideLoader()
            self?.view?.displayError(withMessage: Wordings.error.rawValue)
        })
    }
}

// MARK: - MediaDetailsPresenterProtocol Conformance
extension MediaDetailsPresenter: MediaDetailsPresenterProtocol {}

// MARK: Lifecycle
extension MediaDetailsPresenter {
    func attach(_ view: MediaDetailsViewProtocol) {
        assert(self.view == nil, "Cannot attach view twice")
        self.view = view
    }

    func viewDidLoad(mediaId: Int, mediaType: MediaType) {
        fetch(mediaWithId: String(mediaId), andType: mediaType)
    }
}

// MARK: - Actions
extension MediaDetailsPresenter {
    func playTrailer(withURL url: URL?) {
        guard let url = url else {
            assertionFailure("URL should be valid, otherwise no action should fire")
            return
        }
        view?.openWebview(withURL: url)
    }
}
