//
//  MediaDetailsModel.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

struct MediaDetails {
    struct Member {
        let name: String
        let character: String
        let profilePath: String?
    }

    struct Similar {
        let title: String
        let posterPath: String?
    }

    let title: String
    let summary: String
    let genre: String?
    let backdropPath: String?
    let trailer: URL?
    let cast: [Member]
    let similar: [Similar]
}
