//
//  SimilarMediaCollectionViewCell.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 7/10/20.
//

import UIKit

class SimilarMediaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}

// MARK: - Load
extension SimilarMediaCollectionViewCell {
    func configureView(with similar: MediaDetailsUIModel.Similar) {
        titleLabel.text = similar.title
        posterImageView.loadImage(from: similar.posterURL)
    }
}
