//
//  SimilarMediaTableViewCell.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 7/10/20.
//

import UIKit

class SimilarMediaTableViewCell: UITableViewCell {
    @IBOutlet weak var similarMediaCollectionView: UICollectionView!

    private var similar: [MediaDetailsUIModel.Similar]?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - Load
extension SimilarMediaTableViewCell {
    func configureView(with similar: [MediaDetailsUIModel.Similar]) {
        self.similar = similar
        similarMediaCollectionView.dataSource = self
        similarMediaCollectionView.reloadData()
    }
}

extension SimilarMediaTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        similar?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = String(describing: SimilarMediaCollectionViewCell.self)
        let reusableCell = similarMediaCollectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        guard let cell = reusableCell as? SimilarMediaCollectionViewCell else {
            assertionFailure("Expected `\(SimilarMediaCollectionViewCell.self)` type for reuse identifier \(identifier)")
            return SimilarMediaCollectionViewCell()
        }
        guard let similar = similar else {
            assertionFailure("Similar media should be set at this point")
            return SimilarMediaCollectionViewCell()
        }
        cell.configureView(with: similar[indexPath.row])
        return cell
    }
}
