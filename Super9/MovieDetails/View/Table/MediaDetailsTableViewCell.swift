//
//  MediaDetailsTableViewCell.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 7/10/20.
//

import UIKit

class MediaDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var playTrailerButton: UIButton!

    private var playTrailerAction: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - Load
extension MediaDetailsTableViewCell {
    // MARK: Config
    /// A configuration object for `MediaDetailsTableViewCell`.
    struct Config {
        let title: String
        let summary: String
        let genre: String?
        let backdropImage: URL?
        let playTrailerAction: (() -> Void)?
    }

    func configureView(with config: Config) {
        titleLabel.text = config.title
        summaryLabel.text = config.summary
        genreLabel.text = config.genre
        backdropImageView.loadImage(from: config.backdropImage)

        playTrailerAction = config.playTrailerAction
        if let _ = config.playTrailerAction {
            playTrailerButton.isHidden = false
        } else {
            playTrailerButton.isHidden = true
        }
    }
}

// MARK: - Actions
extension MediaDetailsTableViewCell {
    @IBAction func playTrailer(_ sender: UIButton) {
        playTrailerAction?()
    }
}
