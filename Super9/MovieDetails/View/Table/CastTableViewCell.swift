//
//  CastTableViewCell.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 7/10/20.
//

import UIKit

class CastTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - Load
extension CastTableViewCell {
    func configureView(with member: MediaDetailsUIModel.Member) {
        imageView?.loadImage(from: member.profileURL)
        textLabel?.text = member.name
        detailTextLabel?.text = member.character
    }
}
