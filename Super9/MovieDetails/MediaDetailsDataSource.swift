//
//  MediaDetailsDataSource.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import Foundation

func makeYouTubeURL(key: String?) -> URL? {
    guard let key = key else { return nil }
    return URL(string: "https://www.youtube.com/watch?v=\(key)")
}

// MARK: Extension Helpers
fileprivate enum VideoType: String {
    case Trailer
}

fileprivate enum VideoSite: String {
    case YouTube
}

extension Movie {
    var trailerKey: String? {
        return videos?.results?.first {
            $0.type == VideoType.Trailer.rawValue && $0.site == VideoSite.YouTube.rawValue
        }?.key
    }
}

extension TVShow {
    var trailerKey: String? {
        videos?.results?.first {
            $0.type == VideoType.Trailer.rawValue && $0.site == VideoSite.YouTube.rawValue
        }?.key
    }
}

// MARK: Errors
enum DetailsDataSourceError: Error {
    case generic
}

// MARK: - MediaDetailsDataTask
protocol MediaDetailsDataTask {
    func resume()
}

extension URLSessionDataTask: MediaDetailsDataTask {}

// MARK: - MediaDetailsSession
protocol MediaDetailsSession {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void

    func detailsDataTask(with url: URL, completionHandler: @escaping Handler) -> MediaDetailsDataTask
}

extension URLSession: MediaDetailsSession {
    func detailsDataTask(with url: URL, completionHandler: @escaping Handler) -> MediaDetailsDataTask {
        dataTask(with: url, completionHandler: completionHandler)
    }
}

// MARK: - Mappers
extension Movie {
    func toMediaDetails() -> MediaDetails? {
        guard
            let title = title,
            let summary = overview,
            let cast = credits?.cast,
            let similar = similar?.results
        else {
            return nil
        }
        return MediaDetails(
            title: title,
            summary: summary,
            genre: genres?.first?.name,
            backdropPath: backdropPath,
            trailer: makeYouTubeURL(key: trailerKey),
            cast: cast.compactMap { member in
                guard let name = member.name, let character = member.character else {
                    return nil
                }
                return MediaDetails.Member(
                    name: name,
                    character: character,
                    profilePath: member.profilePath)
            },
            similar: similar.compactMap { movie in
                guard let title = movie.title else {
                    return nil
                }
                return MediaDetails.Similar(title: title, posterPath: movie.posterPath)
            }
        )
    }
}

extension TVShow {
    func toMediaDetails() -> MediaDetails? {
        guard
            let title = name,
            let summary = overview,
            let cast = credits?.cast,
            let similar = similar?.results
        else {
            return nil
        }
        return MediaDetails(
            title: title,
            summary: summary,
            genre: genres?.first?.name,
            backdropPath: backdropPath,
            trailer: makeYouTubeURL(key: trailerKey),
            cast: cast.compactMap { member in
                guard let name = member.name, let character = member.character else {
                    return nil
                }
                return MediaDetails.Member(
                    name: name,
                    character: character,
                    profilePath: member.profilePath)
            },
            similar: similar.compactMap { movie in
                guard let title = movie.title else {
                    return nil
                }
                return MediaDetails.Similar(title: title, posterPath: movie.posterPath)
            }
        )
    }
}

// MARK: - MediaDetailsDataSource
final class MediaDetailsDataSource: MediaDetailsDataSourceProtocol {
    private let config: Config
    private let urlSession: MediaDetailsSession

    init(config: Config = Config.shared, urlSession: MediaDetailsSession = URLSession.shared) {
        self.config = config
        self.urlSession = urlSession
    }

    private func fetchDetails(
        forMovieWithId id: String, onSuccess: @escaping (MediaDetails) -> Void, onError: @escaping (Error) -> Void
    ) {
        let detailsURL = URL.from(config: config, path: API.movie.rawValue + "\(id)", queryItems: [
            URLQueryItem(name: "append_to_response", value: "videos,credits,similar")
        ])
        guard let url = detailsURL else {
            onError(DetailsDataSourceError.generic)
            return
        }

        urlSession.detailsDataTask(with: url) { data, response, error in
            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data, let movie = try? JSONDecoder().decode(Movie.self, from: data),
                let details = movie.toMediaDetails()
            else {
                DispatchQueue.main.async {
                    onError(SearchDataSourceError.generic)
                }
                return
            }

            DispatchQueue.main.async {
                onSuccess(details)
            }
        }.resume()
    }

    private func fetchDetails(
        forTvShowWithId id: String, onSuccess: @escaping (MediaDetails) -> Void, onError: @escaping (Error) -> Void
    ) {
        let detailsURL = URL.from(config: config, path: API.tv.rawValue + "\(id)", queryItems: [
            URLQueryItem(name: "append_to_response", value: "videos,credits,similar")
        ])
        guard let url = detailsURL else {
            onError(DetailsDataSourceError.generic)
            return
        }

        urlSession.detailsDataTask(with: url) { data, response, error in
            guard
                error == nil,
                let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                let data = data, let show = try? JSONDecoder().decode(TVShow.self, from: data),
                let details = show.toMediaDetails()
            else {
                DispatchQueue.main.async {
                    onError(SearchDataSourceError.generic)
                }
                return
            }

            DispatchQueue.main.async {
                onSuccess(details)
            }
        }.resume()
    }

    func fetchDetails(
        for type: MediaType, withId id: String, onSuccess: @escaping (MediaDetails) -> Void, onError: @escaping (Error) -> Void
    ) {
        switch type {
        case .movie:
            fetchDetails(forMovieWithId: id, onSuccess: onSuccess, onError: onError)
        case .tvShow:
            fetchDetails(forTvShowWithId: id, onSuccess: onSuccess, onError: onError)
        }
    }
}
