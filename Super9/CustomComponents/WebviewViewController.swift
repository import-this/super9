//
//  WebviewViewController.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 7/10/20.
//

import UIKit
import WebKit

class WebviewViewController: UIViewController, WKUIDelegate {
    private lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        return webView
    }()

    private let url: URL

    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let myRequest = URLRequest(url: url)
        webView.load(myRequest)
    }
}
