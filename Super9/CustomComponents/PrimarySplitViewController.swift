//
//  PrimarySplitViewController.swift
//  Super9
//
//  Created by Poulimenos, Vasileios, Vodafone Greece on 4/10/20.
//

import UIKit

/// A custom SplitViewController that displays the Master view first
/// (contrast to the default iOS behavior which displays the Detail view).
class PrimarySplitViewController: UISplitViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredDisplayMode = .allVisible
        self.delegate = self
    }
}

extension PrimarySplitViewController: UISplitViewControllerDelegate {
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController
    ) -> Bool {
        // Return true to display the Master view first.
        true
    }
}
